﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controler : MonoBehaviour {
    bool canjump =true;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("d"))
        {

            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(1000f * Time.deltaTime, 0));
            gameObject.GetComponent<Animator>().SetBool("moving", true);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
            gameObject.GetComponent<Rigidbody2D>().drag = 0;
        }
        if (Input.GetKeyUp("d")|| Input.GetKeyUp("a"))
        {

            
            if (canjump == false)
            {
                gameObject.GetComponent<Rigidbody2D>().drag = 0;
            }
            else
            {
                gameObject.GetComponent<Rigidbody2D>().drag=20;
            }
        }
        


        if ( Input.GetKey("a"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1000f * Time.deltaTime, 0));
            gameObject.GetComponent<Animator>().SetBool("moving", true);
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
            gameObject.GetComponent<Rigidbody2D>().drag = 0;
        }

        




        if (!Input.GetKey("a")&& !Input.GetKey("d"))
        {
            gameObject.GetComponent<Animator>().SetBool("moving", false);
        }

        if (Input.GetKeyDown("space") && canjump)
        {
            canjump = false;
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,500f));
            gameObject.GetComponent<Animator>().SetBool("jumping", false);
            gameObject.GetComponent<Rigidbody2D>().drag = 0;
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 2;    
        }

        
        
        

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0f));

        gameObject.GetComponent<Animator>().SetBool("jumping", true);
        if (collision.transform.tag == "ground")
        {
            gameObject.GetComponent<Rigidbody2D>().drag = 20;
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
            canjump = true;
        }
       
    }

}
